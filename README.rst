===================================
aiotumblr - async Tumblr API client
===================================
`aiotumblr` is a Tumblr OAuth 1.0a API client built on top of `asyncio`, `aiohttp` and `oauthlib`. It includes all
documented API endpoints, and allows to add additional endpoints as well using extensions.