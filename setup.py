# encoding=utf-8
from setuptools import setup, find_packages

setup(
    name='aiotumblr',
    version='0.2.1',
    description='Tumblr API client on top of aiohttp and oauthlib',
    author='Lena',
    author_email='arlena@hubsec.eu',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.7',
    ],
    packages=find_packages(),
    install_requires=['aiotumblr-core>=0.1.4.3', 'aiotumblr-public>=0.2.1'],  # FIXME: 0.2 update this to >=0.2
)
