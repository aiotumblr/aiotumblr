from aiotumblr_core import TumblrClient
from aiotumblr_ext.extensions.public import PublicAPI

__all__ = ['TumblrClient']

TumblrClient.register_extension(PublicAPI)
